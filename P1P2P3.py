import pandas as pd
from pandas import Series, DataFrame # Not used.

import datetime
import random
import numpy
import math # Not used.


class Bond:
    def __init__(self, term):
        if term == "short" or term == "Short": # In case the first letter can be inputted as either lower case or capital
            self.MinTerm = 3  # the unit for Minimum Term is year
            self.MinAmount = 500  # the unit for Minimum Amount is dollar
            self.YrInterest = 0.01  # Yearly Interest Rate
        if term == "long" or term == "Long": # In case the first letter can be inputted as either lower case or capital
            self.MinTerm = 5  # the unit for Minimum Term is year
            self.MinAmount = 1000  # the unit for Minimum Amount is dollar
            self.YrInterest = 0.03  # Yearly Interest Rate


class Stock:
    def __init__(self, stockname, datafile):
        self.stockname = stockname
        self.df = pd.read_table(datafile, sep=";") # read data from csv file and separate a row into different columns by ";"
        self.indexed = self.df.set_index('Date')
        self.date = self.df["Date"]
        self.histprice = self.df["Close"] # Note in all following codes, "Close" means "close price", which is used as the price for stocks

    def get_close_price(self, date):
        return self.indexed.loc[date, "Close"]

    def get_period_return(self, dt_start, dt_end): # dt_start means start date, dt_end means end date
        a = self.get_close_price(dt_start)
        b = self.get_close_price(dt_end)
        x = (b - a) / a
        return x # x is the rate of return from specified start date until end date


class Investor:
    def __init__(self, mode, budget, dt_start, dt_end):
        self.mode = mode  # mode is one of defensive, mixed, or aggressive
        self.start = dt_start
        self.t = self.start  # one particular time
        self.time = [self.t]  # track the time when he or she make investment
        self.end = dt_end
        self.initBudget = budget
        self.capital = [self.initBudget]  # track his or her capital at time t
        self.bond = [0, 0]  # number of bonds for short-term and long-term resp.
        self.stk = [0, 0, 0, 0, 0, 0, 0]  # number of stocks for FDX, GOOGL, IBM, KO, MS, NOK, XOM resp.
        self.stocklist = ["FDX", "GOOGL", "IBM", "KO", "MS", "NOK", "XOM"]
        self.calender = pd.read_table("G:\PythonProject\Data\IBM.csv", sep=";")["Date"]

    def str2datetime(self, date):
        return datetime.datetime.strptime(date, "%Y-%m-%d") # convert string to datetime

    def datetime2str(self, dt):
        return dt.strftime("%Y-%m-%d") # convert datetime to string

    ## In the following def, we can find out "n days before a certain date".

    def daysBefore(self, date, n):
        givendate = self.str2datetime(date)
        beforedate = (givendate - datetime.timedelta(days=n))
        return self.datetime2str(beforedate)

    ## In the following def, we consider two cases when considering how to count "n years from certain date".
    ## A very special case is 02/29 since it does NOT exist every year. In this case, we switch to 03/01.
    ## For example, what is the day for 4 yours from 2008/02/29? It gives 2012/03/01.

    def yearsAfter(self, date, n):
        dt = self.str2datetime(date)
        year = dt.year
        month = dt.month
        day = dt.day
        if month == 2 and day == 29:
            d = str(year + n) + "-" + "03" + "-" + "01"
        else:
            d = str(year + n) + date[4:]
        return d

    def findWDay(self, date):
        if self.str2datetime(date) <= self.str2datetime("2005-01-03"):
            d = "2005-01-03"
        elif self.str2datetime(date) >= self.str2datetime("2016-12-30"):
            d = "2016-12-30"
        else:
            li = []
            for i in self.calender.index:
                li.append(self.str2datetime(self.calender[i]))
            # print(li)
            n = 0
            # type(date)
            dd = self.datetime2str(self.str2datetime(date))
            while datetime.datetime.strptime(dd, "%Y-%m-%d") not in li:
                n = n + 1
                dd = self.daysBefore(dd, 1)
            d = dd
        return d

    def buyBonds(self, date_start, capital):
        if self.str2datetime(date_start) == self.str2datetime(self.time[-1]):
            if random.random() < 0.5:
                purchase = int(capital[-1] / Bond("Short").MinAmount)
                self.bond[0] = purchase
                residual = capital[-1] - Bond("Short").MinAmount * purchase
                capital.append(residual + self.bond[0] * Bond("Short").MinAmount * (1 + Bond("Short").YrInterest) ** 3)
                self.t = self.yearsAfter((date_start), 3)
                self.time.append(self.t)
            else:
                purchase_long = int(capital[-1] / Bond("Long").MinAmount)
                self.bond[1] = purchase_long
                self.bond[0] = 0
                if capital[-1]  - purchase_long * Bond("Long").MinAmount >= 500:
                    self.bond[0] = 1
                residual = capital[-1] - Bond("Short").MinAmount * self.bond[0] - Bond("Long").MinAmount * self.bond[1]
                capital.append(residual + self.bond[0] * Bond("Short").MinAmount * (1 + Bond("Short").YrInterest) ** 3 + self.bond[1] * Bond("Long").MinAmount * (1 + Bond("Long").YrInterest) ** 5)
                self.t = self.yearsAfter((date_start), 5)
                self.time.append(self.t)
        else:
            print("Error! The date given is not the day of the previous investment!")



    ## In the followng def, we show up the following things:
    ## (1) "print(self.bond)" gives a sequence of lists showing the numbers of short term bonds and long term bonds investors purchase;
    ## (2) the date they buy these bonds;
    ## (3) the amount of money they own in total at the corresponding date in (2).
    ## For (1), here are further explanations. If investors buy short term bonds, then the lists can only look like [n,0]
    ## where n denotes number of the short term bonds to be purchased, and 0 means no long term bonds can be purchased.
    ## However, if investors buy long term bonds, then the lists can have two possible forms, either [0,n] or [1,n].
    ## The understanding of [0,n] is similar as that for [n,0] above.
    ## [1,n] means investors not only buy n long term bonds but also holds one and ONLY one short term bond.
    ## Why? Denote m as all available amount of money that can be used to purchase for next 5 year term.
    ## Then if m-1000n >= 500, investors can choose to purchase ONLY one short term bond, and we get [1,n];
    ## If m-1000n < 500, investors will not be able to purchase any short term bond, and we get [0,n].
    ## Another consideration arises naturally. If the investors purchase both long term bonds and short term bonds,
    ## then short term bonds only need 3 years to reach its maturity, but long term bonds need 5 years.
    ## We then include a strong assumption here:
    ## After their maturity, short term bonds will not create more interests in the remaining 2 years.

    def onlyBuyBonds(self):
        if random.random() < 0.5:
            while self.str2datetime(self.time[-1]) <= self.str2datetime(self.end):
                purchase = int(self.capital[-1] / Bond("Short").MinAmount)
                # print(Bond("Short").MinAmount)
                self.bond[0] = purchase  # qty bought
                residual = self.capital[-1] - Bond("Short").MinAmount * self.bond[0]
                self.capital.append(
                    residual + self.bond[0] * Bond("Short").MinAmount * (1 + Bond("Short").YrInterest) ** Bond(
                        "Short").MinTerm)
                self.t = self.yearsAfter((self.time[-1]), 3)
                self.time.append(self.t)
                # print(self.bond)
            self.time.pop()
            self.capital.pop()
        else:
            while self.str2datetime(self.time[-1]) <= self.str2datetime(self.end):
                purchase_long = int(self.capital[-1] / Bond("Long").MinAmount)
                self.bond[1] = purchase_long  # qty bought
                self.bond[0] = 0
                if self.capital[-1] - purchase_long * Bond("Long").MinAmount >= 500:
                    self.bond[0] = 1
                residual = self.capital[-1] - Bond("Short").MinAmount * self.bond[0] - Bond("Long").MinAmount * \
                                                                                       self.bond[1]
                self.capital.append(
                    residual + self.bond[0] * Bond("Short").MinAmount * (1 + Bond("Short").YrInterest) ** Bond(
                        "Short").MinTerm + self.bond[1] * Bond("Long").MinAmount * (
                    1 + Bond("Long").YrInterest) ** Bond("Long").MinTerm)
                self.t = self.yearsAfter((self.time[-1]), 5)
                self.time.append(self.t)
                # print(self.bond)
            self.time.pop()
            self.capital.pop()



    ## In the following def, we consider stocks.
    ## We firstly specify the way to buy stocks. We have 7 stocks in total.
    ## We first generate a random number (denoted as r) from [0.0, 1.0], and then multiply it by 7.
    ## 7r is therefore a number from [0.0, 10.0] with equally probability to fall into one of the segments [0.0, 1.0), [1.0, 2.0), ..., [9.0, 10.0],
    ## where each piece of segment represents ONE share of ONE stocks.
    ## Once 7r falls into a certain segment, then we buy ONE share of the stock represented by this piece of segment.
    ## We keep this procedure, until there is no more than 100 dollars remaining in our pocket.
    ## We assume that this procedure is completed in an infinitely short period of time to be ignored.
    ## Also, notice that the highest possible value for NOK (Nokia's stock) in our desired 12-year-long time period
    ## never exceeds 42. This ensures that we can choose stocks all the way until we have less than 100 dollars.

    def buyStocks(self, date_1, capital, date_2 = "2016-12-30"):
        if self.str2datetime(date_1) == self.str2datetime(self.time[-1]):
            FDX = Stock("FDX", "G:\PythonProject\Data\FDX.csv")
            GOOGL = Stock("GOOGL", "G:\PythonProject\Data\GOOGL.csv")
            IBM = Stock("IBM", "G:\PythonProject\Data\IBM.csv")
            KO = Stock("KO", "G:\PythonProject\Data\KO.csv")
            MS = Stock("MS", "G:\PythonProject\Data\MS.csv")
            NOK = Stock("NOK", r"G:\PythonProject\Data\NOK.csv")  # Unicode
            XOM = Stock("XOM", "G:\PythonProject\Data\XOM.csv")
            money = capital[-1]
            while money > 100:
                random_num = 7 * random.random()
                if random_num >= 0 and random_num < 1 and FDX.get_close_price(date_1) <= money:
                    self.stk[0] = self.stk[0] + 1
                    money = money - FDX.get_close_price(date_1)
                elif random_num >= 1 and random_num < 2 and GOOGL.get_close_price(date_1) <= money:
                    self.stk[1] = self.stk[1] + 1
                    money = money - GOOGL.get_close_price(date_1)
                elif random_num >= 2 and random_num < 3 and IBM.get_close_price(date_1) <= money:
                    self.stk[2] = self.stk[2] + 1
                    money = money - IBM.get_close_price(date_1)
                elif random_num >= 3 and random_num < 4 and KO.get_close_price(date_1) <= money:
                    self.stk[3] = self.stk[3] + 1
                    money = money - KO.get_close_price(date_1)
                elif random_num >= 4 and random_num < 5 and MS.get_close_price(date_1) <= money:
                    self.stk[4] = self.stk[4] + 1
                    money = money - MS.get_close_price(date_1)
                elif random_num >= 5 and random_num < 6 and NOK.get_close_price(date_1) <= money:
                    self.stk[5] = self.stk[5] + 1
                    money = money - NOK.get_close_price(date_1)
                elif random_num >= 6 and random_num < 7 and XOM.get_close_price(date_1) <= money:
                    self.stk[6] = self.stk[6] + 1
                    money = money - XOM.get_close_price(date_1)
            self.capital.append(
                money + FDX.get_close_price(date_2) * self.stk[0] + GOOGL.get_close_price(date_2) * self.stk[
                    1] + IBM.get_close_price(date_2) * self.stk[2] + KO.get_close_price(date_2) * self.stk[
                    3] + MS.get_close_price(date_2) * self.stk[4] + NOK.get_close_price(date_2) * self.stk[
                    5] + XOM.get_close_price(date_2) * self.stk[6])
            self.t = date_2
            self.time.append(self.t)
        else:
            print("Error! The date given is not the day of the previous investment!")

    def buyMixed(self):
        if random.random() < 0.5:
            self.onlyBuyBonds()
        else:
            self.buyStocks("2005-01-03", self.capital, "2016-12-30")


    def invest(self):
        if self.mode == "defensive" or self.mode == "Defensive":
            self.onlyBuyBonds()
        elif self.mode == "aggressive" or self.mode == "Aggressive":
            self.buyStocks("2005-01-03", self.capital, "2016-12-30")
        elif self.mode == "mixed" or self.mode == "Mixed":
            self.buyMixed()


#### Date (find, transformation) functions:
def str2datetime(date):
    return datetime.datetime.strptime(date, "%Y-%m-%d") # convert string to datetime

def datetime2str(dt):
    return dt.strftime("%Y-%m-%d") # convert datetime to string

## In the following def, we can find out "n days before a certain date".
def daysBefore(date, n):
    givendate = str2datetime(date)
    beforedate = (givendate - datetime.timedelta(days=n))
    return datetime2str(beforedate)

def daysAfter(date, n):
    givendate = str2datetime(date)
    afterdate = (givendate + datetime.timedelta(days=n))
    return datetime2str(afterdate)

## In the following def, we consider two cases when considering how to count "n years from certain date".
## A very special case is 02/29 since it does NOT exist every year. In this case, we switch to 03/01.
## For example, what is the day for 4 yours from 2008/02/29? It gives 2012/03/01.
def yearsAfter(date, n):
    dt = str2datetime(date)
    year = dt.year
    month = dt.month
    day = dt.day
    if month == 2 and day == 29:
        d = str(year + n) + "-" + "03" + "-" + "01"
    else:
        d = str(year + n) + date[4:]
    return d

def findAfterWDay(date):
    if str2datetime(date) <= str2datetime("2005-01-03"):
        d = "2005-01-03"
    elif str2datetime(date) >= str2datetime("2016-12-30"):
        d = "2016-12-30"
    else:
        li = []
        calender = pd.read_table("G:\PythonProject\Data\IBM.csv", sep=";")["Date"]
        for i in calender.index:
            li.append(str2datetime(calender[i]))
        # print(li)
        n = 0
        # type(date)
        dd = datetime2str(str2datetime(date))
        while datetime.datetime.strptime(dd, "%Y-%m-%d") not in li:
            n = n + 1
            dd = daysAfter(dd, 1)
        d = dd
    return d

def findBeforeWDay(date):
    if str2datetime(date) <= str2datetime("2005-01-03"):
        d = "2005-01-03"
    elif str2datetime(date) >= str2datetime("2016-12-30"):
        d = "2016-12-30"
    else:
        li = []
        calender = pd.read_table("G:\PythonProject\Data\IBM.csv", sep=";")["Date"]
        for i in calender.index:
            li.append(str2datetime(calender[i]))
        # print(li)
        n = 0
        # type(date)
        dd = datetime2str(str2datetime(date))
        while datetime.datetime.strptime(dd, "%Y-%m-%d") not in li:
            n = n + 1
            dd = daysBefore(dd, 1)
        d = dd
    return d

def notWorkingDay(date):
    li = []
    calender = pd.read_table("G:\PythonProject\Data\IBM.csv", sep=";")["Date"]
    for i in calender.index:
        li.append(str2datetime(calender[i]))
    dd = datetime2str(str2datetime(date))
    return datetime.datetime.strptime(dd, "%Y-%m-%d") not in li


############ Test ############

### Test of functions
## Bond Class
print(Bond("short").MinTerm)
print(Bond("Long").MinAmount)

## Stock Class
FDXtest = Stock("FDX", "G:\PythonProject\Data\FDX.csv")
print(FDXtest.get_close_price("2005-01-03"))
IBMtest = Stock("IBM", "G:\PythonProject\Data\IBM.csv")
print(IBMtest.get_period_return("2005-01-03","2016-12-30"))

## Investor Class
Anne = Investor("defensive", 15000, "2000-01-03", "2050-01-04")

# String to Datetime type transformation
onedate = Anne.str2datetime("2005-01-03")
print(type(onedate))
print(onedate)

# Datetime to String type transformation
onedate = Anne.datetime2str(onedate)
print(type(onedate))
print(onedate)

# Compute the date which is N days before
print(Anne.daysBefore(onedate, 1))

# Compute the date which is N years after
print(Anne.yearsAfter(onedate, 1))
partic = "2008-02-29"
print(Anne.yearsAfter(partic, 3))

# Find the Working day before a randomly given date
print(Anne.findWDay("2005-01-01"))
print(Anne.findWDay("2005-1-3"))
print(Anne.findWDay("2008-2-29"))
print(Anne.findWDay("2008-03-1"))
print(Anne.findWDay("2016-12-31"))


### Test of investors
##  Part1: Defensive investor Anne, initial capital $15000, from 2000/01/03 - 2050/01/04

Anne = Investor("defensive", 15000, "2000-01-03", "2050-01-04")
Anne.invest()
print("The term of investment is from %s to %s." % ("2000-01-03", "2050-01-04"))
print("The type of investor is %s." % ("defensive"))
print("The initial capital for investment is $%s." % str(15000))
print("The final quantity for short-term bond is %s, and that for the long-term bond is %s." % (Anne.bond[0],Anne.bond[1]))
print("The final capital for investment is $%s." % str(Anne.capital[-1]))
for i in range(0,len(Anne.time)):
    print("His or her capital at %s is $%s." % (Anne.time[i],Anne.capital[i]))


### Test of investors

##  Part2: Aggressive investor Bill, initial capital $15000, from 2005/01/03 - 2016/12/30

Bill = Investor("aggressive", 15000, "2005-01-03", "2016-12-30")
Bill.invest()
print("The term of investment is from %s to %s." % ("2005-01-03", "2016-12-30"))
print("The type of investor is %s." % ("aggressive"))
print("The initial capital for investment is $%s." % str(15000))
print("The quantity for each stock is as follows:")
for i in range(0,len(Bill.stocklist)):
    print("\t%s:  %s" % (Bill.stocklist[i],Bill.stk[i]))
print("The final capital for investment is $%s." % str(Bill.capital[-1]))


##  Part3: Mixed investor Chris, initial capital $15000, from 2005/01/03 - 2016/12/30

Chris = Investor("mixed", 15000, "2005-01-03", "2016-12-30")
Chris.invest()
print("The term of investment is from %s to %s." % ("2005-01-03", "2016-12-30"))
print("The type of investor is %s." % ("mixed"))
print("The initial capital for investment is $%s." % str(15000))
print("The final capital for investment is $%s." % str(Chris.capital[-1]))


##  Plus: Test one period & two period bond investment

David = Investor("mixedRedistributed", 15000, "2005-01-03", "2016-12-30")
David.buyBonds(David.time[-1], David.capital)
print("The term of investment is from %s to %s." % (David.time[-2], David.time[-1]))
print("The initial capital for investment is $%s." % David.capital[-2])
print("The quantity for short-term bond is %s, and that for the long-term bond is %s." % (David.bond[0],David.bond[1]))
print("The capital at the end of this investment is $%s." % str(David.capital[-1]))
David.buyBonds(David.time[-1], David.capital)
print("The term of investment is from %s to %s." % (David.time[-2], David.time[-1]))
print("The initial capital for investment is $%s." % David.capital[-2])
print("The quantity for short-term bond is %s, and that for the long-term bond is %s." % (David.bond[0],David.bond[1]))
print("The capital at the end of this investment is $%s." % str(David.capital[-1]))


##  Plus: Test one period bond + one period stock investment

Francios = Investor("mixedRedistributed", 15000, "2005-01-03", "2016-12-30")
print(Francios.time[-1])
if notWorkingDay(Francios.time[-1]):
    Francios.time[-1] = findAfterWDay(Francios.time[-1])
    print("Due to holiday, it moves to the next business day: %s"  % Francios.time[-1])
Francios.buyBonds(Francios.time[-1], Francios.capital)
print(Francios.time[-1])
if notWorkingDay(Francios.time[-1]):
    Francios.time[-1] = findAfterWDay(Francios.time[-1])
    print("Due to holiday, it moves to the next business day: %s"  % Francios.time[-1])
Francios.buyStocks(Francios.time[-1], Francios.capital)
print("The term of investment is from %s to %s." % (Francios.time[0], Francios.time[-1]))
print("The initial capital for investment is $%s." % Francios.capital[0])
print("The capital at the end of this investment is $%s." % str(Francios.capital[-1]))


############ Simulation ############
n = 1000

### Simulation 1

## 1000 defensive investor:

defensive_investor = [Investor("defensive", 15000, "2005-01-03", "2016-12-30") for i in range(n)]
capitalforall = []
for i in range(n):
    defensive_investor[i].invest()
    capitalforall.append(defensive_investor[i].capital[-1])
meanreturn = numpy.mean(capitalforall)
#print(capitalforall)
print("The mean return for 1000 defensive investors is $%s." % meanreturn)


## 1000 aggressive investor:

aggressive_investor = [Investor("aggressive", 15000, "2005-01-03", "2016-12-30") for i in range(n)]
capitalforall = []
for i in range(n):
    aggressive_investor[i].invest()
    capitalforall.append(aggressive_investor[i].capital[-1])
meanreturn = numpy.mean(capitalforall)
#print(capitalforall)
print("The mean return for 1000 aggressive investors is $%s." % meanreturn)


## 1000 mixed investor:

mixed_investor = [Investor("mixed", 15000, "2005-01-03", "2016-12-30") for i in range(n)]
capitalforall = []
for i in range(n):
    mixed_investor[i].invest()
    capitalforall.append(mixed_investor[i].capital[-1])
meanreturn = numpy.mean(capitalforall)
# print(capitalforall)
print("The mean return for 1000 mixed investors is $%s." % meanreturn)


### Simulation 2
# Returns for defensive & aggressive investors remains the same.
# Mixed investor with redistributed behavior
mixedRe_investor = [Investor("mixedRedistributed", 15000, "2005-01-03", "2016-12-30") for i in range(n)]
capitalforall = []
for i in range(n):
    while str2datetime(mixedRe_investor[i].time[-1]) < str2datetime(mixedRe_investor[i].end):
        if notWorkingDay(mixedRe_investor[i].time[-1]):
            mixedRe_investor[i].time[-1] = findAfterWDay(mixedRe_investor[i].time[-1])
        if random.random() < 0.5:
            mixedRe_investor[i].buyStocks(mixedRe_investor[i].time[-1], mixedRe_investor[i].capital, "2016-12-30")
        elif str2datetime(yearsAfter((mixedRe_investor[i].time[-1]),5)) < str2datetime(mixedRe_investor[i].end):
            mixedRe_investor[i].buyBonds(mixedRe_investor[i].time[-1], mixedRe_investor[i].capital)
        # print(mixedRe_investor[i].time[-1])
    capitalforall.append(mixedRe_investor[i].capital[-1])
meanreturn = numpy.mean(capitalforall)
#print(capitalforall)
print("The mean return for 1000 mixed investors with redistributed behavior is $%s." % meanreturn)


### Simulation 3

## 1000 defensive investor:

defensive_investor = [Investor("defensive", 150000, "2005-01-03", "2016-12-30") for i in range(n)]
capitalforall = []
for i in range(n):
    defensive_investor[i].invest()
    capitalforall.append(defensive_investor[i].capital[-1])
meanreturn = numpy.mean(capitalforall)
#print(capitalforall)
print("The mean return for 1000 defensive investors is $%s." % meanreturn)


## 1000 aggressive investor:

aggressive_investor = [Investor("aggressive", 150000, "2005-01-03", "2016-12-30") for i in range(n)]
capitalforall = []
for i in range(n):
    aggressive_investor[i].invest()
    capitalforall.append(aggressive_investor[i].capital[-1])
meanreturn = numpy.mean(capitalforall)
#print(capitalforall)
print("The mean return for 1000 aggressive investors is $%s." % meanreturn)


## 1000 Mixed investor with redistributed behavior:

mixedRe_investor = [Investor("mixedRedistributed", 150000, "2005-01-03", "2016-12-30") for i in range(n)]
capitalforall = []
for i in range(n):
    while str2datetime(mixedRe_investor[i].time[-1]) < str2datetime(mixedRe_investor[i].end):
        if notWorkingDay(mixedRe_investor[i].time[-1]):
            mixedRe_investor[i].time[-1] = findAfterWDay(mixedRe_investor[i].time[-1])
        if random.random() < 0.5:
            mixedRe_investor[i].buyStocks(mixedRe_investor[i].time[-1], mixedRe_investor[i].capital, "2016-12-30")
        elif str2datetime(yearsAfter((mixedRe_investor[i].time[-1]),5)) < str2datetime(mixedRe_investor[i].end):
            mixedRe_investor[i].buyBonds(mixedRe_investor[i].time[-1], mixedRe_investor[i].capital)
        # print(mixedRe_investor[i].time[-1])
    capitalforall.append(mixedRe_investor[i].capital[-1])
meanreturn = numpy.mean(capitalforall)
#print(capitalforall)
print("The mean return for 1000 mixed investors with redistributed behavior is $%s." % meanreturn)
