import P1P2P3


n = 1000

### Simulation 1

## 1000 defensive investor:

defensive_investor = [Investor("defensive", 15000, "2005-01-03", "2016-12-30") for i in range(n)]
capitalforall = []
for i in range(n):
    defensive_investor[i].invest()
    capitalforall.append(defensive_investor[i].capital[-1])
meanreturn = numpy.mean(capitalforall)
#print(capitalforall)
print("The mean return for 1000 defensive investors is $%s." % meanreturn)


## 1000 aggressive investor:

aggressive_investor = [Investor("aggressive", 15000, "2005-01-03", "2016-12-30") for i in range(n)]
capitalforall = []
for i in range(n):
    aggressive_investor[i].invest()
    capitalforall.append(aggressive_investor[i].capital[-1])
meanreturn = numpy.mean(capitalforall)
#print(capitalforall)
print("The mean return for 1000 aggressive investors is $%s." % meanreturn)


## 1000 mixed investor:

mixed_investor = [Investor("mixed", 15000, "2005-01-03", "2016-12-30") for i in range(n)]
capitalforall = []
for i in range(n):
    mixed_investor[i].invest()
    capitalforall.append(mixed_investor[i].capital[-1])
meanreturn = numpy.mean(capitalforall)
# print(capitalforall)
print("The mean return for 1000 mixed investors is $%s." % meanreturn)


### Simulation 2
# Returns for defensive & aggressive investors remains the same.
# Mixed investor with redistributed behavior
mixedRe_investor = [Investor("mixedRedistributed", 15000, "2005-01-03", "2016-12-30") for i in range(n)]
capitalforall = []
for i in range(n):
    while str2datetime(mixedRe_investor[i].time[-1]) < str2datetime(mixedRe_investor[i].end):
        if notWorkingDay(mixedRe_investor[i].time[-1]):
            mixedRe_investor[i].time[-1] = findAfterWDay(mixedRe_investor[i].time[-1])
        if random.random() < 0.5:
            mixedRe_investor[i].buyStocks(mixedRe_investor[i].time[-1], mixedRe_investor[i].capital, "2016-12-30")
        elif str2datetime(yearsAfter((mixedRe_investor[i].time[-1]),5)) < str2datetime(mixedRe_investor[i].end):
            mixedRe_investor[i].buyBonds(mixedRe_investor[i].time[-1], mixedRe_investor[i].capital)
        # print(mixedRe_investor[i].time[-1])
    capitalforall.append(mixedRe_investor[i].capital[-1])
meanreturn = numpy.mean(capitalforall)
#print(capitalforall)
print("The mean return for 1000 mixed investors with redistributed behavior is $%s." % meanreturn)


### Simulation 3

## 1000 defensive investor:

defensive_investor = [Investor("defensive", 150000, "2005-01-03", "2016-12-30") for i in range(n)]
capitalforall = []
for i in range(n):
    defensive_investor[i].invest()
    capitalforall.append(defensive_investor[i].capital[-1])
meanreturn = numpy.mean(capitalforall)
#print(capitalforall)
print("The mean return for 1000 defensive investors is $%s." % meanreturn)


## 1000 aggressive investor:

aggressive_investor = [Investor("aggressive", 150000, "2005-01-03", "2016-12-30") for i in range(n)]
capitalforall = []
for i in range(n):
    aggressive_investor[i].invest()
    capitalforall.append(aggressive_investor[i].capital[-1])
meanreturn = numpy.mean(capitalforall)
#print(capitalforall)
print("The mean return for 1000 aggressive investors is $%s." % meanreturn)


## 1000 Mixed investor with redistributed behavior:

mixedRe_investor = [Investor("mixedRedistributed", 150000, "2005-01-03", "2016-12-30") for i in range(n)]
capitalforall = []
for i in range(n):
    while str2datetime(mixedRe_investor[i].time[-1]) < str2datetime(mixedRe_investor[i].end):
        if notWorkingDay(mixedRe_investor[i].time[-1]):
            mixedRe_investor[i].time[-1] = findAfterWDay(mixedRe_investor[i].time[-1])
        if random.random() < 0.5:
            mixedRe_investor[i].buyStocks(mixedRe_investor[i].time[-1], mixedRe_investor[i].capital, "2016-12-30")
        elif str2datetime(yearsAfter((mixedRe_investor[i].time[-1]),5)) < str2datetime(mixedRe_investor[i].end):
            mixedRe_investor[i].buyBonds(mixedRe_investor[i].time[-1], mixedRe_investor[i].capital)
        # print(mixedRe_investor[i].time[-1])
    capitalforall.append(mixedRe_investor[i].capital[-1])
meanreturn = numpy.mean(capitalforall)
#print(capitalforall)
print("The mean return for 1000 mixed investors with redistributed behavior is $%s." % meanreturn)
